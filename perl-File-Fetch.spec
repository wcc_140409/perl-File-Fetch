Name:           perl-File-Fetch
Version:        0.56
Release:        4
Summary:        A generic file fetching mechanism
License:        GPL+ or Artistic
URL:            https://perldoc.perl.org/File/Fetch.html
Source0:        http://www.cpan.org/authors/id/B/BI/BINGOS/File-Fetch-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  gcc git make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76 perl(strict)
BuildRequires:  perl(Carp) perl(constant) perl(Cwd) perl(File::Basename) perl(File::Copy) perl(File::Path) 
BuildRequires:  perl(File::Spec) >= 0.82 perl(File::Spec::Unix) perl(File::Temp) perl(FileHandle) perl(IPC::Cmd) >= 0.42
BuildRequires:  perl(Locale::Maketext::Simple) perl(Module::Load::Conditional) >= 0.66 perl(Params::Check) >= 0.07 perl(vars)
BuildRequires:  perl(Data::Dumper) perl(IO::Socket::INET) perl(lib) perl(Test::More) perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version)) perl(File::Spec) >= 0.82 
Requires:       perl(IPC::Cmd) >= 0.42 perl(Locale::Maketext::Simple) perl(Module::Load::Conditional) >= 0.66 perl(Params::Check) >= 0.07

Suggests:       git-core perl(LWP) perl(LWP::Protocol::https) rsync
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\((File::Spec|IPC::Cmd|Module::Load::Conditional|Params::Check)\\)$

%description
File::Fetch is a generic file fetching mechanism.
It allows you to fetch any file pointed to by a ftp , http , file , git or rsync uri by a number of different means.
See the HOW IT WORKS section further down for details.

%package_help

%prep
%autosetup  -n File-Fetch-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%make_build

%install
%make_install
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc README
%{perl_vendorlib}/*

%files help
%doc CHANGES
%{_mandir}/man3/*

%changelog
* Sun Sep 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.56-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise changelog

* Sun Sep 15  2019 shidongdong <shidongdong5@huawei.com> - 0.56-3
- Package init 
